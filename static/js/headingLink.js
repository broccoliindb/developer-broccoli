let icon = '<i class="fas fa-link"></i>';
let headings = document.querySelectorAll('h2, h3');
for (let i = 0; i < headings.length; i++) {
  let id = headings[i].id;
  let iconLink = '<a class="header-icon-link" href="#' + id + '">' + icon + '</a> ';
  let headingWithIconLink = `${headings[i].innerHTML} ${iconLink}`;
  headings[i].innerHTML = headingWithIconLink;
}