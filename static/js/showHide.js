const showHideButtons = document.querySelectorAll('.displayHideInner')
const showHideHandler = (evt) => {
  const button = evt.target
  const target = button.nextElementSibling || button.parentElement.nextElementSibling
  if (target) {
    target.classList.toggle('show')
  }
}
showHideButtons.forEach(btn => {
  btn.addEventListener('click', showHideHandler)
})