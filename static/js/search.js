let pagesIndex, searchIndex

const suggestions = document.querySelector(".suggestions");
const searchInput = document.querySelector("#searchInput");

const fetchJSONFile = (path, callback) => {
  const httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = function () {
    if (httpRequest.readyState === 4) {
      if (httpRequest.status === 200) {
        const data = JSON.parse(httpRequest.responseText);
        if (callback) callback(data);
      }
    }
  };
  httpRequest.open('GET', path);
  httpRequest.send();
}

const isKorean = (term) => {
  const numUnicode = term.charCodeAt(0);
  if (44032 <= numUnicode && numUnicode <= 55203) return true;
  return false;
}

const koreanOption = {
  encode: false,
  split: /\s+/,
  tokenize: "full"
}

const engOption = {
  encode: "advanced",
  tokenize: "reverse",
  suggest: true,
  cache: true,
}

const flexIndex = (term) => {
  if (isKorean(term)) {
    return new FlexSearch(koreanOption)
  } else {
    return new FlexSearch(engOption)
  }
}

const flexSearch = (term) => {
  searchIndex = flexIndex(term)
  for (let i = 0; i < pagesIndex.length; i++) {
    searchIndex.add(i, pagesIndex[i].contents);
  }
  return Promise.resolve(searchIndex)
}

const init = () => {
  fetchJSONFile(baseUrl + 'index.json', function (data) {
    pagesIndex = data
  })
}

const show_results = async (evt) => {
  const target = evt.target
  const value = target.value;
  const index = await flexSearch(value)
  const results = index.search(value, 100);
  let li, title, description, shortContents, anchor;
  let i = 0, len = results.length;
  const fragment = document.createDocumentFragment()
  while (suggestions.children.length) {
    [...suggestions.children].forEach((item) => {
      suggestions.removeChild(item)
    })
  }
  for (; i < len; i++) {

    li = document.createElement('li')
    title = document.createElement("div");
    title.classList.add('suggestions__title')
    title.innerText = pagesIndex[results[i]].title
    description = document.createElement("div");
    anchor = document.createElement('a')
    anchor.setAttribute('href', pagesIndex[results[i]].permalink)
    description.classList.add('suggestions__description')
    description.innerText = pagesIndex[results[i]].description
    shortContents = document.createElement("div");
    shortContents.classList.add('suggestions__shortContents')
    let start = pagesIndex[results[i]].contents.indexOf(value)
    start = start - 10 >= 0 ? start - 10 : 0
    const replaceContent = pagesIndex[results[i]].contents.replace(value, `<code>${value}</code>`)
    shortContents.innerHTML = replaceContent.substring(start)
    anchor.appendChild(title)
    anchor.appendChild(description)
    anchor.appendChild(shortContents)
    li.appendChild(anchor)
    fragment.appendChild(li);

  }
  suggestions.appendChild(fragment)
}

window.addEventListener('load', init)

searchInput.addEventListener("input", show_results, true);

