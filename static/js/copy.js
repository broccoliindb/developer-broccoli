const highlightTables = document.querySelectorAll('.highlight table')

const showToast = (status, target) => {
  const parent = target.parentElement
  const span = document.createElement('span')
  if (status === 'ok') {
    span.classList.add('showCopy')
    span.innerText = '복사됨!!!'
    parent.appendChild(span)
    setTimeout(function removeToast() { parent.removeChild(span) }, 200)
  } else {
    span.classList.add('showCopy')
    span.classList.add('error')
    span.innerText = '복사실패ㅠㅠ!!!'
    parent.appendChild(span)
    setTimeout(function removeToast() { parent.removeChild(span) }, 200)
  }
}

const successCopyHandler = (evt) => {
  showToast('ok', evt.trigger)
  evt.clearSelection();
}

const errorCopyHandler = (evt) => {
  showToast('error', evt.trigger)
}

if (highlightTables && highlightTables.length) {
  [...highlightTables].forEach(table => {
    const copyToClipboard = document.createElement('span')
    copyToClipboard.classList.add('copy-to-clipboard')
    copyToClipboard.title = "클립보드로복사"
    table.appendChild(copyToClipboard)
  })

  let clipboard = new ClipboardJS('.copy-to-clipboard', {
    text: function (trigger) {
      const target = trigger.previousElementSibling.querySelector('td:last-child pre code').textContent
      return target.replace(/^\$\s/gm, '');
    }
  })
  clipboard.on('success', successCopyHandler)
  clipboard.on('error', errorCopyHandler)
}




