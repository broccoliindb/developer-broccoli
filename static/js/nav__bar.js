const container = document.querySelector('.container')
const bar = document.querySelector('.bar')
if (container && bar) {
  const navContainer = document.querySelector('.navContainer')
  const navOffSide = document.querySelector('.navOffSide')
  const breadcrumb = document.querySelector('.breadcrumb')

  if (bar) {
    bar.addEventListener('click', function () {
      navContainer.classList.toggle('showMenu')
      container.classList.toggle('showMenu')
      breadcrumb.classList.toggle('showMenu')
    })
    navOffSide.addEventListener('click', function (evt) {
      const target = evt.target
      if (target.closest('.bar')) return
      breadcrumb.classList.remove('showMenu')
      navContainer.classList.remove('showMenu')
      container.classList.remove('showMenu')
    })
  }
}