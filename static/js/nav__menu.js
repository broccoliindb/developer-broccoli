const navSubject = document.querySelector(".subjects");
const aside = document.querySelector(".aside");
const headerContainer = document.querySelector(".headerContainer");
const subjectArrow = navSubject.querySelector(".subjectArrow");
if (navSubject && aside && headerContainer) {
  const reset = () => {
    aside.classList.remove("showSubject");
    document.body.classList.remove("showSubject");
    headerContainer.style["z-index"] = 0;
    subjectArrow.innerHTML = '<i class="fas fa-chevron-up"></i>';
  };
  const undo = aside.querySelector(".undo");
  navSubject.addEventListener("click", function () {
    aside.classList.toggle("showSubject");
    document.body.classList.toggle("showSubject");
    if (document.body.classList.contains("showSubject")) {
      headerContainer.style["z-index"] = 1;
      subjectArrow.innerHTML = '<i class="fas fa-chevron-down"></i>';
    } else {
      headerContainer.style["z-index"] = 0;
      subjectArrow.innerHTML = '<i class="fas fa-chevron-up"></i>';
    }
  });
  undo.addEventListener("click", reset);
}
