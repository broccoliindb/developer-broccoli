---
title: "About Me"
displayTitle: ""
date: 2020-09-26T11:36:20+09:00
isCJKLanguage: true
draft: false
categories: []
description: ""
tags: []
weight: 0
disqus_identifier: "4e5a8a9883b89992d96a6be4b26686b2"
disqus_title: "About Me"
disqus_url: "4e5a8a9883b89992d96a6be4b26686b2"
menu: true
---
## experience

|기간|업무|소속|
|---|---|---|
|2015.9 ~ 2016.5|- BESTCARE 2.0 해외버전 고도화<br>- WHO 몽골, HeartScore 교육 및 통역담당 |이지케어텍|
|2016.6 ~ 2016.11|서울대 본원 차세대 프로젝트|이지케어텍|
|2016.12 ~ 2017.8|미국 Aurora 병원 차세대 프로젝트|이지케어텍|
|2017.9 ~ 2018.5|분당 서울대 병원 운영팀|이지케어텍|
|2018.6 ~ 2020.5|HIS Cloud 프로젝트|이지케어텍|

## skills

- 경험해 보았으나 더이상 관심을 갖을 생각이 없는 기술
  - dotnet
  - wpf
  - oracle
  - ember

- 자신있는 스킬 및 앞으로도 계속 관심을 갖을 만한 기술
  - javascript
  - html
  - css
  - nodejs
  - express
  - pug, ejs
  - mongodb
  - hugo
  - jenkins
  - bash,zsh
  - docker
  - linux
  - git
  
## project

## side project

## play ground