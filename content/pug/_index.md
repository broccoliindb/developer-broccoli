---
title: "Pug"
displayTitle: "pug"
date: 2020-09-26T16:07:51+09:00
isCJKLanguage: true
draft: false
categories: []
description: "nodejs의 view engine중 하나"
tags: []
weight: 0
disqus_identifier: "13c49709fae8428bd0ac47cb37aefdf9"
disqus_title: "Pug"
disqus_url: "13c49709fae8428bd0ac47cb37aefdf9"
---
# pug

퍼그는 템플릿 엔진입니다.

multi pages로 웹을 구현할 때 일일히 모든 html을 페이지마다 다 구현할 필요 없이, 아래 기능들을 통하여 손쉽게 views단을 구현할 수 있습니다.

- extends
- include
- mixin
- block

템플릿 엔진으로는 pug이외에 ejs도 있지만 개인적으로는 아래 이유로 pug를 더 선호 합니다.

- ejs는 block이 없다.
- ejs는 일일히 <% %> 과 같은 시작과 끝을 달아줘야한다.

## install & setting

### 설치

```js
npm install pug
```

### setting

```js
const path = require('path')
// node의 시작 파일을 가령 index.js라 할때, index.js에
app.set('view engine', 'pug')

// 디폴트 views의 폴더는 views인데 변경을 만약 view 폴더를 원할 때는 아래와 같이 세팅을 해주면 된다.
app.set('views', path.join(__dirname, 'views'))
```

### Basic Usage

SPA가 아니라면 라우팅에 따라 각각의 화면이 필요하다. 그리고 pug또한 Multi page를 사용하기 때문에 각각의 화면이 필요한데, 
view engine들은 이러한 경우 효율적인 html구현이 이루어지도록 도와주는 여러 기능을 제공하고 있다.

- extends
- include
- mixin
- block

이 그와 같은 기능을 도와준다. views가 아래와 같이 구성되었다고 가정할 때 예를 들어보자

```html
//views folder
views
├──layouts
│     ├── baseOf.pug
│     ├── footer.pug
│     └── head.pug
├──partials
│     ├── part1.pug
│     └── part2.pug
└──mixins
       └── movie.pug

```

## extends

모든 화면마다 공통적으로 활용되는 골격의 경우 extends를 통해서 layout을 미리 구현하고 이것을 활용할 수 있다.

```html
// baseOf.pug
doctype html
html
  head
  body
    block content
  footer
```

```html
// index.pug
extends layouts/baseOf
block content
  h1 welcome home
```

## include

만약 부분적으로 활용하는 html을 구현 및 재활용을 할때 사용함.

```html
// head.pug
head
  meta(charset="UTF-8")
  meta(name="viewport", content="width=device-width, initial-scale=1.0")
  title test page
```

이렇게 부분을 활용한 html을 구현했다면 아래 baseOf.pug가 아래와 같이 변경된다.

```html
// baseOf.pug
doctype html
html
  include head
  body
    block content
  footer
```

## mixin

부분적으로 활용하는 html이란 부분은 include기능과 동일하지만 동적으로 컨텐츠를 변경할 때 사용한다.

```html
mixin movieMixin(movie={})
  h3
    a(href=`/${movie.id}`)
      span=movie.title
```

index페이지에 무비리스트를 바인딩되어 정보가 넘어왔고 영화각각의 제목과 해당 링크롤 바인딩한 위의 믹스인이 위와 같을때 믹스인을 포함해보자.

```html
extends layouts/baseOf
include mixins/movie

block content
  h1 welcome home
  ul
    each movie in movies
      li
        +movieMixin(movie)
  
```

## block

블락은 content내용을 동적으로 변경하고자 할때 사용한다. baseOf에 body에 아래와 같이 block content를 구현한 부분이 대체된다.

```html
// baseOf.pug
doctype html
html
  include head
  body
    block content
  footer
```

아래와 같이 block되어있는 부분이 baseOf의 block content와 대체된다.

```html
extends layouts/baseOf
include mixins/movie

block content
  h1 #{pageTitle}
  ul
    each movie in movies
      li
        +movieMixin(movie)
```

## javascript

view engine은 템플릿에 자바스크립트를 작성할 수있도록 해준다.

- #{} : 내분에 자바스크립트를 활용할 수 있다.
- \- var input = [] : 와 같이 dash를 사용해서 자바스크립트를 활용할 수있다.
- loop : 템플릿에 바로 사용가능
  - each val, key in {1: 'one', 2: 'two'}
  - while 조건절
- p=title : 와 같이 innerText로 데이터를 넣을 수도 있다.
