---
title: "Nodejs"
displayTitle: "Node"
date: 2020-09-16T11:33:41+09:00
isCJKLanguage: true
draft: false
categories: []
description: "오픈소스, 크로스 플랫폼이며, 개발자가 모든 종류의 서버사이드 도구들과 어플리케이션을 js로 만들수 있도록 해주는 런타임환경"
tags: []
weight: 0
disqus_identifier: "af4b61d775dd1b084c1edc9440ed8bb8"
disqus_title: "Nodejs"
disqus_url: "af4b61d775dd1b084c1edc9440ed8bb8"
---
# Node.js

브라우저에 한정되어있던 자바스크립트를 브라우저 바깥으로 꺼내서 서버사이드 에서도 활용할 수 있도록 해줌.

- 오픈소스 크로스플랫폼
- 실시간 웹 어플리케이션에 어울린다.
- npm이라는 패키지 매니저를 통해 수많은 재사용성 패키지에 접근할 수있다.
- 큰 개발 생태계와 커뮤니티를 가지고 있다.

