---
title: "Ajax"
displayTitle: "Ajax"
date: 2020-10-30T23:16:17+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Asynchronous Javascript and XML"
tags: []
weight: 0
disqus_identifier: "05c5c701faa5fd92f5777ae1a8fa72c3"
disqus_title: "Ajax"
disqus_url: "05c5c701faa5fd92f5777ae1a8fa72c3"
---
## Ajax

Ajax는 Asynchronous Javascript and XML 의 줄임말이고 아래의 기능을 한다.

- 어떤 정보를 서버로부터 가지고 오거나 보낼때 사용함
- 서버에 요청을 보내고 서버가 응답을 줄때까지 기다린다.
- `페이지를 새로고침하지 않아도 페이지의 정보를 응답을 통해 변경한다. =>>>> Allows us to build SPAs`
