---
title: "Concept"
displayTitle: "기본 컨셉 및 용어정리"
date: 2020-10-30T23:28:17+09:00
isCJKLanguage: true
draft: false
categories: []
description: ""
tags: []
weight: 1
disqus_identifier: "eb79c117cc23e2de3c24d525a2735d79"
disqus_title: "Concept"
disqus_url: "eb79c117cc23e2de3c24d525a2735d79"
---
## 기본 컨셉 및 용어 정리

### XML : Extensible markup language

과거에 브라우저와 서버 사이에 정보교환시 사용된 공식포맷이지만 지금은 사용이 거의 되지 않고 요즘은 보통 *JSON*이 사용된다.

### XMLHttpRequest

짧게 XHR이라고도 하는데 정보를 보내거나 받을 때 사용하는 함수이다. 이것을 통해 Ajax가 가능해졌음.

// 좀 더 정리할 필요있음.