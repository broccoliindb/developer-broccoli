---
title: "Quick Start"
displayTitle: "Ajax Quick Start"
date: 2020-10-30T23:58:18+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Ajax를 사용하는 방법"
tags: []
weight: 2
disqus_identifier: "76ff5265853fd508d7626f5be761aaf6"
disqus_title: "Quick Start"
disqus_url: "76ff5265853fd508d7626f5be761aaf6"
---
## Ajax Quick Start

브라우저에서 제공해주는 기본방식의 사용법과 axios의 사용법을 확인해보자

### XMLHttpRequest

```js
const request = new XMLHttpRequest()
request.addEventListener('load', e => { /*do sth*/})
request.open(method, url)
request.send()
```

### fetch

```js
fetch(url, option)
  .then(response => response.json())
  .then(data => console.log(data))
```

### axios

```js
axios.get(url)
axios.post(url, payload)
axios.put(url, payload)
axios.delete(url, payload)

axios({
  method: 'get',
  url: url,
  responseType: type
})
```
