---
title: "React"
displayTitle: "REACT"
date: 2020-10-27T12:29:41+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트JS의 대한 자료"
tags: []
weight: 0
disqus_identifier: "ad7a3c124f5fffb6cde86d664c8e6671"
disqus_title: "React"
disqus_url: "ad7a3c124f5fffb6cde86d664c8e6671"
---
# React

- 페이스북이 만든 프론트엔드 라이브러리이다. 신뢰할 수 있다.
- 시장이 매우크다. 익히기 좋은 배경을 가지고 있다.
- 가상돔을 사용한다.

ℹ️ [react vs vue in 2020](https://www.mindk.com/blog/react-vs-vue/) : react와 vue의 비교자료

> 리액트는 현재 가장 큰 커뮤니티 및 사용시장을 가지고 있다. 
> 개인적으로 프레임워크나 라이브러리는 영원하지 않으므로 개발자로서 종속적이 되면 안된다고 생각한다. 단지 도구로서 효율적으로 이용할 수 있게끔 익히는것이 좋다.
