---
title: "Router"
displayTitle: "React Router"
date: 2020-10-28T13:42:04+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트 라우터 사용법의 대한 문서"
tags: ["react router"]
weight: 5
disqus_identifier: "61c3dfbd6b2e3d44260758b0dbd5e635"
disqus_title: "Router"
disqus_url: "61c3dfbd6b2e3d44260758b0dbd5e635"
---
## 리액트 라우터

리액트는 SPA를 위한 프론트 엔드 라이브러리이다. SPA는 Single Page Application이다. 이것은 기존 웹방식인 여러 Pages가 필요가 없을 뿐더러 기존 백엔드쪽가 가지고 있던 라우팅기능을 프론트가 가져가게 한다.

- single page 속에는 여러 컴포넌트들이 있는데 라우팅에 따라 특정 컴포넌트들만 사용자에게 노출이 된다.
- 리액트에서 Router는 HashRouter와 BrowserRouter가 있다.
- Route에 기본적으로 react-router에 의해 전달되는 default props가 있고 이것들을 통해 해당 컴포넌트에 정보를 전달할 수 있다.
- `Route, Link 에 props를 추가하지 않는다. 추가해야한다면 render prop을 활용한다. `

### react-router-dom 설치

리액트 라우팅을 위해 `react-router-dom`을 설치하자

```react
npm install react-router-dom
```

### Props

라우팅을 하기 위해서는 2가지의 props 정보가 필요하다. 

- *어떤 경로 url*을 할 것인지
- *어떤 component*를 사용할 것인지

```react
<Route path="/path" component={컴포넌트}>
```

이 때 라우팅이 아래와 같이 겹치는 부분이 있다면 겹치는 부분 모두 렌더링이 된다. 이것을 방지하기 위해서는 `exact={true}` 라는 속성값을 추가해주면 된다.

```react
<Route path='/' exact={true} component={Home}>
<Route path='/about' component={About}>
```

### Link

html에 a tag가 있다 이것은 화면을 refresh해준다. 즉 페이지를 leave함을 의미하며 기존에 있던 single page가 죽고 새로 load됨을 의미한다.

따라서 react에서는 a tag를 사용하지 않고 `Link`를 사용한다.

```react
<Link to='/'>Home</Link>
<Link to='{{
  pathname: '/about',
  state: {
    year,
    title,
    summery,
    poster,
    genres
  }
}}'>About</Link>
```

ℹ️ `Link는 Router 내부에서 사용해야 동작한다.`

### withRouter

withRouter는 컴포넌트를 감쌀수가 있는데 Router로 감싼것이기 때문에 *location, match, history props`를 감쌓진 컴포넌트에 전달할 수가 있다.

```react
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";

// A simple component that shows the pathname of the current location
class ShowTheLocation extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  render() {
    const { match, location, history } = this.props;

    return <div>You are now at {location.pathname}</div>;
  }
}

// Create a new component that is "connected" (to borrow redux
// terminology) to the router.
const ShowTheLocationWithRouter = withRouter(ShowTheLocation);
```

### React Router로 렌더링하는 컴포넌트에 prop전달하기

```react
<Route
  path='/education'
  render={() => <Education education={data} />}/>
```
