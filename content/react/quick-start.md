---
title: "Quick Start"
displayTitle: "React Quick Start"
date: 2020-10-28T15:38:43+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트 입문 빨리 시작하기"
tags: ["react quick start"]
weight: 2
disqus_identifier: "0b5c368a5ad426dd311526102ba7d914"
disqus_title: "Quick Start"
disqus_url: "0b5c368a5ad426dd311526102ba7d914"
---

## React Quick Start

### create-react-app 설치

리액트 앱 빨리 시작하기 위해 초기 세팅을 위해 설치하기
리액트를 세팅하기 위해 부수적으로 설치해줘야하는 세팅이 모두 이미 설치되어있는 모듈이다.

```react
npx create-react-app 프로젝트이름
```

npx로 설치하는 이유는 npx로 설치하면 설치 모듈을 최신 버전으로 다운로드하고 설치한 뒤 설치파일을 삭제하기 때문
*일일히 사용자가 유지관리할 필요가 없다.*
