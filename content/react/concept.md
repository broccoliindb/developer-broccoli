---
title: "Concept"
displayTitle: "기본 컨셉 및 용어정리"
date: 2020-10-27T12:44:06+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트 사용에 있어 필요한 용어정리"
tags: []
weight: 1
disqus_identifier: "560937ce34679ea84d60ca5a16d9b9f8"
disqus_title: "Concept"
disqus_url: "560937ce34679ea84d60ca5a16d9b9f8"
---
## 기본 컨셉 및 용어정리

### 컴포넌트

- SPA를 이루는 작은 단위들
- 리액트에서 컴포넌트는 html을 반환하는 함수라고 생각하자
  - 컴포넌트가 클래스인 경우 html을 랜더링하는 클래스라고 생각하자
- 리액트에서는 `렌더시에 하나의 컴포넌트만 렌더링한다.` 따라서 여러개의 컴포넌트를 렌더링할 때는 부모자식관계처럼 포함하도록 해서 `최종적으로는 하나의 컴포넌트만 렌더링하게끔 하자`.

### JSX

- Javascript과 HTML의 조합이며 리액트에만 특화된 개념
- props를 key value로 `부모컴포넌트로부터 자식컴포넌트로` 전달 할 수 있다.
  - props를 es6의 destructuring assignment가능함
  - props를 통해 코드의 재사용을 이용하면 효율적임

#### 기본형태

```react
function Food(props) {
  return <h1> I like </h1>
}

function App() {
  return (
    <div>
      <Food prop1={value: 'a'} prop2='value2'></Food>
    </div>
  )
}
```

### element Unique

리액트에서는 element들이 각각 유일해야한다. 하지만 만약 컴포넌트를 재사용하여 이 element들이 유일성을 갖고 있지 않다면 에러가 발생한다. 

따라서 컴포넌트들의 유일성을 보장해야한다.

```react
<Food key={id}>
```

### prop-types

컴포넌트 props의 type의 validation을 체크해주는 라이브러리

해당 prop이 필수로 있어야하는 경우 혹은 타입이 다른 경우 console에 표시한다.

예를 들어 Food라는 컴포넌트가 있다고 할 때

```react
<Food name={item.name} picture={item.picture} ratings={item.ratings}/>
```

```react
Food.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
  rating: PropTypes.string.isRequired,  
}
```

위와 같이 PropTypes를 정의해주고 이때 `PropTypes`는 키워드이다.

#### Type  종류

거의 모든 타입을 정의할 수있다. 아래 링크를 통해서 사용법을 확인 할 수 있다.

[공식문서링크](https://reactjs.org/docs/typechecking-with-proptypes.html)

```
array
bool
func
number
object
string
symbol
node
element
instanceOf({Class})
oneOf([])
oneOfType([])
arrayOf(PropTypes.number)
objectOf(PropTypes.number)
shape({
  color: PropTypes.string,
  fontSize: PropTypes.number
})
exact({
  color: PropTypes.string,
  fontSize: PropTypes.number
})
any
...
```

### state

state는 object이고 키워드이며 리액트에서 state내부에 변경될 데이터를 담는 공간으로 활용한다.

- state의 값은 직접 변경하면 안된다.
- setState()를 사용하여 변경해야한다
  - state값이 변경된다.
  - `render() 호출되어 통해 변경된 state값이 적용`된다.
  - render시 state의 데이터 `값만 변경`된다.
  - *현재상태*를 나타내는 state는 `current`라는 키워드릴 이용한다.
- 초기부터 state에 사용할 모든 데이터를 선언할 필요는 없지만 하는게 좋은 방법이다.

```react
class App extends React.Component {
  state = {
    count: 0
  }
  add = () => {
    this.setState(current => ({count: current.count + 1}))
  }
  minus = () => {
    this.setState(current => ({count: current.count + 1}))
  }
  render() {
    return (
      <div>
        <h1>this is {this.state.count}</h1>
        <button onClick={this.add}>Add</button>
        <button onClick={this.minus}>Minus</button>
      </div>
    )
  }
}
```