---
title: "React Hook"
displayTitle: "React Hook"
date: 2020-11-04T14:27:16+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트 훅의 대한 기본개념 및 정리"
tags: [react, hook]
weight: 0
disqus_identifier: "75291f3adf257a31c8194b98a7affeea"
disqus_title: "React Hook"
disqus_url: "75291f3adf257a31c8194b98a7affeea"
---

## React Hook

리액트 Hook은 16.8부터 등장한 새로운 개념이다.

***class를 만들지 않으면서 state와 리액트 특징을 사용***할 수 있게 해준다.

ℹ️ 즉 functional component에 state를 가질 수 있게 해줌. 

- => react 함수형 프로그래밍으로 GET IN
- 함수형 프로그램은 가독성과 작성이 더 쉽다고 여겨짐
- recompose + Alpha = React Hooks

