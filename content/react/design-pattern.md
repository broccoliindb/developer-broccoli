---
title: "Design Pattern"
displayTitle: "React 디자인 패턴"
date: 2020-11-01T12:45:14+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트 디자인 패턴의 대한 문서"
tags: []
weight: 0
disqus_identifier: "af50c6ed6704b55816634ba8d48797a6"
disqus_title: "Design Pattern"
disqus_url: "af50c6ed6704b55816634ba8d48797a6"
---

## react 디자인 패턴

디자인 패턴은 유지보수를 하기 위해 용이한 패턴의 개발형태를 의미하는데 리액트 컴포넌트 디자인 패턴을 정리해보자

### Container Presenter Pattern

#### Container

컨테이너는 **`data와 state, api를 가지고`** *로직*을 처리한다.

#### Presenter

프레젠터는 **`api와 data, state에 대한 모든것을 알 필요없이`** *view의 대한 처리*하는 functional component이다

#### 구조 (예: Home Component)

```
Home
╠ HomeContainer.js
╠ HomePresenter.js
╚ index.js
```

✅ index.js

```react
import HomeContainer from './HomeContainer'
export default HomeContainer
```

✅ HomeContainer.js

```react
import React from 'react'
import HomePresenter from './HomePresenter'

export default class extends React.Component {
  state = {
    ...
  }
  render() {
    <HomePresenter />
  }
}
```

✅ HomePresenter.js

```react
import React from 'react'
export default () => {
  ...
}

```