---
title: "Trouble Shooting"
displayTitle: "Trouble Shooting"
date: 2020-11-02T17:42:48+09:00
isCJKLanguage: true
draft: false
categories: []
description: "React Issue 처리"
tags: ["issue", "fix issue", "trouble shooting", "unexpected token"]
weight: 1
disqus_identifier: "6be35560f189fb8d0b3742e41c66ec40"
disqus_title: "Trouble Shooting"
disqus_url: "6be35560f189fb8d0b3742e41c66ec40"
---

## React 이슈

### Unexpected token

![unexpectedtoken](/react/images/unexpectedtoken.png)

위와 같은 에러의 경우 react에러가 아니라 eslint에서 catch된 에러이고 app은 잘 돌아간다. 다만 수정을 원하다면 eslint 자체를 babel로 파싱해서 lint를 시키면된다.

참고로 구글링해보면 *@babel/plugin-proposal-class-properties* 이 모듈을 설치하라고 나오는데, ***해결책이 되지 않는다.***

#### 해결법

✅ .eslintrc.js 

```js
parser: "babel-eslint",
```

이것을 추가하면 해결된다. [참고링크](https://support.glitch.com/t/parsing-error-unexpected-token-app-runs-fine-lint-problem/11952/3)

### Component definition is missing

![componentdefinitionmissing](/react/images/componentdefinitionmissing.png)

위에 에러는 해당라인의 component를 익명함수로 만들고 export했을 때 나타남

#### 해결법

해결라인의 컴포넌트를 익명이 아닌 명칭을 명시적으로 지정하면 해결됨.
