---
title: "Lifecycle"
displayTitle: "React Component LifeCycle"
date: 2020-10-27T15:58:29+09:00
isCJKLanguage: true
draft: false
categories: []
description: "React 컴포넌트 Life Cycle의 대한 문서"
tags: [react, lifecycle]
weight: 3
disqus_identifier: "408f416d13216575947c9d1b31eb0bfe"
disqus_title: "Lifecycle"
disqus_url: "408f416d13216575947c9d1b31eb0bfe"
---

## React Component Life Cycle

리액트 컴포넌트 Life Cycle을 통해 비효율적인 DOM생성 비용을 줄일 수 있다.

보통 아래 음영 및 이탤릭된 부분이 주로 사용되는 라이프 사이클 훅이다.

[컴포넌트 라이프 사이클 공식문서 링크](https://reactjs.org/docs/react-component.html)

### Mounting

컴포넌트가 생성되고 DOM에 삽입됐을 때

1. constructor() : 자바스크립트 생성자
2. static getDerivedStateFromError()
3. *render()* : 컴포넌트가 렌더될 때
4. *componentDidMound()* : 컴포넌트가 마운트 됐을 때, 여기서 data를 fetch를 한다.

### Updating

prop혹은 state의 변경이 있을 때

1. static getDerivedStateFromProps() 
2. shouldComponentUpdate() : 업데이트 할지 말지 결정하는 훅
3. *render()* : 컴포넌트가 렌더될 때
4. getSnapshotBeforeUpdate() : 가장 최근렌더된 결과가 DOM에 진입하기 바로 직전 호출됨
5. *componentDidUpdate()* : 업데이트 되면 바로 호출된다. 현재 props와 직전 props의 비교를 해서 request요청하기 좋은 장소

### Unmounting

컴포넌트가 DOM에서 제거될 때 대표적으로는 `페이지가 변경될 때` 이나 state가 변경됨에 따라 컴포넌트가 교체될때에도 해당됨.

1. componentWillUnmount() : 컴포넌트가 제거되기 직전 호출된다. 여기서 setState()은 일어나는건 좋지 않다.

### Error Handling

라이프 사이클이 되면서 렌더링 되는 와중에 생성자든 자식컴포넌트에서든 어디서든 에러가 발생할 수 있는데 이때 사용하는 훅은 아래와 같다.

1. static getDerivedStateFromError()
2. componentDidCatch()
