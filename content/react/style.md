---
title: "Style"
displayTitle: "React Style"
date: 2020-10-27T17:43:53+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트의 스타일을 정의하는 방법"
tags: []
weight: 4
disqus_identifier: "4c1cc8d7fda42835a079ec6334cb1104"
disqus_title: "Style"
disqus_url: "4c1cc8d7fda42835a079ec6334cb1104"
---
## 리액트 스타일

리액트에서 스타일을 정의하는 방법은 일반적인 방법을 사용해도 된다.

- inline style처럼 element에 바로 style을 입혀도 된다. 권장되진 않는다. 
- 콤포넌트 css파일을 만들어 해당 파일을 해당 컴포넌트에 import한다.
- JSX에 class가 아니라 className을 사용한다. 동일한 의미로써 사용됨.
- JSX에서 lable for속성은 htmlFor를 사용한다. 동일한 의미이다.

*하지만 이와 같은 방법은 일반적인 방법이고 이런 일반적인 방법은 모든 스타일이 전역으로 적용되며 css와 컴포넌트가 각각 개별로 존재한다는 점이다.*

리액트에서는 몇가지 다른 방식으로 스타일을 적용할 수 있다. 해당 방법으로는 `특정스타일이 특정 컴포넌트에만 적용`되도록 할 수있다.

### *.module.css

리액트 app에서는 모듈별로 css, scss(*node-sass를 설치해야한다.*)를 로컬화 할 수 있는데

```
Components
 ╚ Header
     ╠ Header.Js
     ╠ Header.module.css
     ╚ index.js
```

✅ Header.module.css
```react
navList {
  display: flex
}
```

✅ Header.js
```react
import React from 'react'
import style from './Header.module.css'

export default () => {
  <header>
    <ul className={style.navList}>
      <li>
        item1
      </li>
      <li>
        item2
      </li>
      <li>
        item3
      </li>
    </us>
  </header>
}
```

✅ index.js

```react
import Header from './Header'
export default Header
```


위와 같이 구조화 되어있을 때 `Header.module.css`라고 명명하고 object처럼 참조를하면 스타일이 컴포넌트에 로컬적으로 사용된다. 하지만 이 경우 `클래스 명칭을 기억`해야한다

### styled-component

[공식문서링크](https://styled-components.com/docs)

만약 css 클래스 명칭을 기억하기도 싫고 컴포넌트에 1:1로 스타일을 매핑시키고 싶다면 styled-component라는 npm 모듈을 사용할 수 있다.

해당 컴포넌트에 클래스를 모듈화 해서 아래와 같이 사용한다.

#### 설치

```react
npm install styled-components
```

#### 사용법

✅ Header.js

```react
import React from 'react'
import styled from 'styled-components'

const List = styled.ul`
  display: flex;
`

export default () => {
  <header>
    <List>
      <li>
        item1
      </li>
      <li>
        item2
      </li>
      <li>
        item3
      </li>
    </List>
  </header>
}
```

#### extending styles
기존 스타일을 override 할 수도 있다.

```react
// The Button from the last section without the interpolations
const Button = styled.button`
  color: palevioletred;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
`;

// A new component based on Button, but with some override styles
const TomatoButton = styled(Button)`
  color: tomato;
  border-color: tomato;
`;

render(
  <div>
    <Button>Normal Button</Button>
    <TomatoButton>Tomato Button</TomatoButton>
  </div>
);
```

#### createGlobalStyle

global 스타일은 특정 컴포넌트에 포함시키지 않고 전역적으로 사용되길 원하는 범위에 포함시켜준다.

```react
import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'

const GlobalStyle = createGlobalStyle`
  ${reset}
  body {
    color: ${props => (props.whiteColor ? 'white' : 'black')};
  }
`

// later in your app

<React.Fragment>
  <GlobalStyle whiteColor />
  <Navigation /> {/* example of other top-level stuff */}
</React.Fragment>
```
