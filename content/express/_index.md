---
title: "Express"
displayTitle: "express"
date: 2020-09-16T11:35:04+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Fast, unopinionated, minimalist web framework for Node.js"
tags: []
weight: 0
disqus_identifier: "7a9ccd79e0ff381696452d30cc960b92"
disqus_title: "Express"
disqus_url: "7a9ccd79e0ff381696452d30cc960b92"
---
# express

Fast, unopinionated, minimalist web framework for Node.js

- 가장 인기있는 Node 웹 프레임워크
- 여러가지 view engine과 결합할 수 있다.
- express 자체는 가능한 최소한의 기능을 탑재하고 있고 필요한 미들웨어 패키지를 추가하여 사용한다.
