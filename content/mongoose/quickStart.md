---
title: "QuickStart"
displayTitle: "몽구스 Quick Start"
date: 2020-10-04T01:15:39+09:00
isCJKLanguage: true
draft: false
categories: []
description: "몽구스의 간단한 사용법"
tags: []
weight: 0
disqus_identifier: "74fd98dc1d34206bb4cbc38c7de9c196"
disqus_title: "QuickStart"
disqus_url: "74fd98dc1d34206bb4cbc38c7de9c196"
---
# Quick Start

몽구스를 사용하는 최소한의 정보를 활용해서 빠르게 시작해보기

## connection

몽고DB와 몽구스를 커넥션하기 {{< show >}}
connection options 정보
{{< /show >}}
{{< hide >}}

- useNewUrlParser: true 필수로 넣자. 몽고db 드라이버버전 차이로 인해 명시해줄 필요있음. 또한 true시에 포트번호도 반드시 명시해줘야함.
- useFindAndModify: findOneAndUpdate, findOneAndReplace, findOneAndDelete로 세분화됨. 기존findAndModify설정 false해줄 필요있음. 몽구스버전에 따라 경고뜸.

{{< /hide >}}

```js
mongoose.connect(url, {opts});
```

ℹ️ [mongoose Connection 상세 참고 링크](https://mongoosejs.com/docs/connections.html)

### connection example

```js
const mongoose = require("mongoose");
mongoose.connect(
  'mongodb://127.0.0.1:27017/dbName',
  {
    useNewUrlParser: true,
    useFindAndModify: false
  }
);

const db = mongoose.connection;

const handleOpen = () => console.log("✅  Connected to DB");
const handleError = error => console.log(`❌ Error on DB Connection:${error}`);

db.once("open", handleOpen);
db.on("error", handleError);
```

## Schema

몽구스는 ODM 역할을 하여 몽고디비의 Document들을 javascript의 Object로 매핑을 해준다. 이때 스키마를 통해 모델의 validation을 체크해주거나 제한할 수 있다.

- 사용자가 작성한 스키마를 바탕으로 DB에 넣기 전에 validation 체크
- 스키마 구성시 인덱스 걸수 있음.
- 생성시 첫번째 인자로 컬렉션 이름을 만드는데 복수형으로 만든다.

```mysql
const model = mongoose.model(`Movie`, MovieSchema);
```

ℹ️ [mongoose Schema 상세 참고 링크](https://mongoosejs.com/docs/guide.html)

### Schema Types

- String
- Number
- Date
- Buffer
- Boolean
- Mixed
- ObjectId
- Array
- Decimal128
- Map

### Schema example

```js
const MovieSchema = mongoose.Schema({
  title: {
    type: String,
    minlength: 3,
    required: "Title required at least 3 characters!!"
  },
  author: String,
  comments: [{ body: String, date: Date }],
  hidden: Boolean,
  meta: {
      votes: Number,
      favs:  Number
    }
  uploadedAt: {
    type: Date,
    default: Date.now
  }
  // HERE YOU HAVE TO CREATE AND COMPLETE THE MOVIE SCHEMA
}, {
  timestamps: true
});

const model = mongoose.model(`Movie`, MovieSchema);

module.exports = model
```

### Schema 부가기능: virtual

실제 Document에는 없지만 반환될 객체에는 있는 가상의 필드를 제공하는 기능

아래 예제 조회시 -> { title: ..., createdAt: ..., likes:
..., 'postDetail': ... }

```js
postSchema.virtual('postDetail').get(functio
n() {
return `이 게시물의 제목은 ${this.title}이고
${this.createdAt.toLocaleString()}에
작성되었습니다.`;
});
```

### Schema 부가기능: pre & post hook

Document에 수행되는 특정 메서드의 전후로 어떤 작업을 수행할 수 있는 기능

```js
userSchema.pre('save', function(next) {
if (!this.email) {
 throw '이메일이 없습니다';
 }
if (!this.createdAt) {
 this.createdAt = new Date();
 }
next();
});
userSchema.post('find', function(result) {
console.log('저장 완료', result);
});
```

### Populate

하나의 Document가 다른 Document의 ObjectId를 갖는 경우가 있다. 그럴때 그 ObjectId를 해당 Document 객체로 치환하는 작업을 populate라고 한다.

ℹ️ [populate 참고링크](https://mongoosejs.com/docs/populate.html)

## Model

### Create

### Modify

### Delete
