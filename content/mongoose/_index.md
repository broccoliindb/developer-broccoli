---
title: "Mongoose"
displayTitle: "mongoose"
date: 2020-10-04T01:09:46+09:00
isCJKLanguage: true
draft: false
categories: []
description: "mongoose의 기본사용법 및 옵션의 대한 문서"
tags: []
weight: 0
disqus_identifier: "c954d6146b5c28d28248dfa2f1ad7dc7"
disqus_title: "Mongoose"
disqus_url: "c954d6146b5c28d28248dfa2f1ad7dc7"
---
# mongoose

- MongoDB ODM중 가장 많이 사용되는 라이브러리

ℹ️ ODM: Object Document Mapping ( RDB의 Object Relational Mapping과 대응되는 개념 ), 이때 `Document`는 MongoDB의 문서를 의미하고 `Object`는 Javascript Object를 의미한다.

- 이 ODM은 몽고DB의 콜렉션에 생성된 Document들을 Object로 매핑해준다.
- 이때 mongoose의 `Schema`를 통해 데이터를 모델링하며, 각 필드의 타입과 validation을 설정하여 매핑할 수 있다. {{< show >}}
 usage
{{< /show >}}
{{< hide >}}

```js
const mongoose = require('mongoose')

const schema = mongoose.Schema({
  ...modeling
})
```
{{< /hide >}}

- populate를 통해 SQL의 join과 비슷한 기능을 흉내낼 수 있다.
- promise based이며 async와 await를 사용할 수 있다.
- 쉬운 Query Builder를 제공한다. {{< show >}}
examples
{{< /show >}}
{{< hide >}}

```Javascript
Model.deleteMany()
Model.deleteOne()
Model.find()
Model.findById()
Model.findByIdAndDelete()
Model.findByIdAndRemove()
Model.findByIdAndUpdate()
Model.findOne()
Model.findOneAndDelete()
Model.findOneAndRemove()
Model.findOneAndReplace()
Model.findOneAndUpdate()
Model.replaceOne()
Model.updateMany()
Model.updateOne()
```

{{< /hide >}}
