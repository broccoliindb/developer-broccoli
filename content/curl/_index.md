---
title: "Curl"
displayTitle: "curl"
date: 2020-09-16T12:17:03+09:00
isCJKLanguage: true
draft: false
categories: []
description: "curl의 대한 기본 사용법과 옵션의 대한 문서"
tags: []
weight: 0
disqus_identifier: "594d60a73b6a0464fc3769c95edbf482"
disqus_title: "Curl"
disqus_url: "594d60a73b6a0464fc3769c95edbf482"
---

# curl

Curl은 손 쉽게 Http 요청을 커맨드라인에서 처리하도록 해주는 프로그램

때문에 많은 Api 문서에서 각 Api를 소개할 때 많이 사용한다.

## 사용법

```js
curl url
```