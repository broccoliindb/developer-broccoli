---
title: "Webpack"
displayTitle: "webpack"
date: 2020-11-04T18:41:23+09:00
isCJKLanguage: true
draft: false
categories: []
description: "웹 어플리케이션을 구성하는 자원들을 위한 모듈 번들러"
tags: [webpack]
weight: 0
disqus_identifier: "598fc2145c5dc6b38cb1a4d9114cef21"
disqus_title: "Webpack"
disqus_url: "598fc2145c5dc6b38cb1a4d9114cef21"
---
## Webpack

웹팩은 최신 프론트엔드에서 많이 사용되는 모듈 번들러로써 웹을 구성하는 assets들의 모듈들을 하나로 번들링해주는 tool이다.

`npm 모듈`을 vanilla js에 활용하는 경우 webpack과 같은 번들러를 사용해서 빌드를 진행해야 활용이 가능하다.
