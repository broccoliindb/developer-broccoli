---
title: "Options"
displayTitle: "webpack 옵션의 대한 설명"
date: 2020-11-05T10:23:49+09:00
isCJKLanguage: true
draft: false
categories: []
description: "webpack options"
tags: []
weight: 0
disqus_identifier: "514386fcd27cbf2562f7b2a3acfa15d4"
disqus_title: "Options"
disqus_url: "514386fcd27cbf2562f7b2a3acfa15d4"
---
## webpack Options

webpack을 적용할 시에는 webpack설정파일(webpack.config.js)을 사용하거나, 아니면 스크립트시에 webpack을 직접 사용할 수 있는데 두 경우 모두 설정을 추가할 수 있다. 아래에는 설정중 하나인 --mode가 포함된 스크립트이다.

`일반적으로 간단한 설정이 아니라면 webpack.config.js를 사용한다.`

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack --mode=none"
  },
```

### 기본 옵션

#### -\-mode 번들링 환경 결정

- -\-mode=production
- -\-mode=production
- -\-mode=production

#### -\-entry 소스의 원본위치

#### -\-output 소스의 출력위치

default 는 dist폴더이지만 다른 경로로 변경할 수 있다.

### webpack.config.js

webpack 설정들을 작성하는 파일인데, 관리 측면에서 스크립트보다는 훨씬 용이하기 때문에 설정옵션이 많이 필요한 경우 사용한다. 파일 위치는 프로젝트 폴더 루트에 만들면 된다.

✅ webpack.config.js

```js
const path = require('path')

module.exports = {
  mode: 'none',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  }
}
```

만약 webpack.config.js 파일을 사용한다면 package.json에 작성했던 옵션들은 제거하면 된다.

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack"
  },
```
