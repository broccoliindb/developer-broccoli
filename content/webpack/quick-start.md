---
title: "Quick Start"
displayTitle: "Webpack Quick Start"
date: 2020-11-05T09:39:25+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Webpack 입문 빨리 시작하기"
tags: [webpack]
weight: 0
disqus_identifier: "9d003a69fc79a59aa7c7c3838a8725e1"
disqus_title: "Quick Start"
disqus_url: "9d003a69fc79a59aa7c7c3838a8725e1"
---
## Webpack Quick Start

node, npm이 설치된 가정에서 순서가 진행됩니다. 아직 node와 npm이 설치가 되지 않은경우 설치를 먼저 진행하세요.

### npm package.json 설치

npm 모듈의 webpack을 사용할 것이기 때문에 가장 먼저 package.json 파일을 만들어 줘야한다.

- 설치시 속성들을 작성해 준다.
- 디폴트로 만들기를 원한다면 enter만 누르거나 명령어 입력시 -y 옵션을 같이 넣어준다.

```js
npm init
```

### webpack, webpack-cli 설치

webpack은 개발환경에서만 사용할 목적이기 때문에 -D옵션을 추가하여 devDependencies에만 종속해준다.

```js
npm install webpack webpack-cli -D
```

### 필요로하는 모듈 설치

예를 들어 redux라는 모듈을 설치하여 js에서 사용해보자

js에서 활용해서 사용할 것이기 때문에 번들링시 포함이 되어야한다. 따라서 -D 옵션을 포함하지 않고 dependencies에 종속해준다.

```js
npm install redux
```

### index.html 파일 및, src 폴더 생성

index.html에 경로를 지정하면 되기 때문에 `파일의 폴더경로는 이렇게 해야한다는 필수적인 사항은 아니다.` 다만 편의상 src폴더아래에 index.js를 생성하자.

![webpack-quick-start](/webpack/images/webpack-quick-start.png)

#### index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Redux Demo</title>
  </head>
  <body>
    <div>
      <button class="add">add</button>
      <span class="count">0</span>
      <button class="minus">minus</button>
    </div>
  </body>
</html>

```

#### index.js

```js
import { createStore } from "redux";

const countEl = document.querySelector(".count");
const addEl = document.querySelector(".add");
const minusEl = document.querySelector(".minus");
let cnt = 0;
countEl.innerText = cnt;
const updateCount = () => {
  countEl.innerText = cnt;
};
const add = () => {
  cnt++;
  updateCount();
};
const minus = () => {
  cnt--;
  updateCount();
};

const store = createStore((cnt = 0));

addEl.addEventListener("click", add);
minusEl.addEventListener("click", minus);
console.log(store);
```

### webpack 번들링(build)하기

package.json에 아래의 build 스크립트를 넣는다. *build*라는 명령어는 키워드가 아니라 사용자가 스크립트를 시행할 속성값이며 `스크립트를 만드는 사람이 임의로 작성하면 된다.`

#### 번들링할 스크립트 작성

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack --mode=none"
  },
```

webpack 번들링시 --mode=none 을 넣지 않고 디폴트로 webpack만 하게 되면 production환경으로 번들링 된다. 그럼 minifying, uglifying이 되어 파일 사이지를 축소시켜주지만 소스를 알아보기 어렵게 된다.
*개발시에는 --mode=none*으로 해주자.

#### 번들링된 파일 포함하기

![webpack-bundle](/webpack/images/webpack-bundle.png)

디폴트로 번들링된 파일은 *프로젝트 폴더 아래에 dist*라는 폴더명으로 번들링된다. 따라서 index.html에 다음과 같이 해당 파일을 포함해준다.

```html
  <body>
    <div>
      <button class="add">add</button>
      <span class="count">0</span>
      <button class="minus">minus</button>
    </div>
    <script src="./dist/main.js"></script>
  </body>
```

이제 해당 파일을 사용할 수 있다.

