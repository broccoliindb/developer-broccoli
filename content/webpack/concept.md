---
title: "Concept"
displayTitle: "컨셉 및 기본 용어 정리"
date: 2020-11-05T09:42:17+09:00
isCJKLanguage: true
draft: false
categories: []
description: "webpack 사용에 있어 기본 용어 정리"
tags: []
weight: 0
disqus_identifier: "b035b512a6246c8f07d617c8d5669296"
disqus_title: "Concept"
disqus_url: "b035b512a6246c8f07d617c8d5669296"
---
## 기본 컨셉 및 용어 정리

### package.json

> This document is all you need to know about what's required in your package.json file.

[공식문서](https://docs.npmjs.com/cli/v6/configuring-npm/package-json)에 위와 같은 설명이 있다. 즉, 내가 만든 소스들을 하나의 패키지(모듈)로 볼때 그 패키지의 필요한 모든 정보를 담아둔 장소로 볼 수 있고 형태는 아래와 같다.

```json
{
  "name": "with-redux",
  "version": "1.0.0",
  "description": "playground redux with vanilla js",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack --mode=none"
  },
  "author": "broccoliiindb",
  "license": "MIT",
  "dependencies": {
    "redux": "^4.0.5"
  },
  "devDependencies": {
    "webpack": "^5.4.0",
    "webpack-cli": "^4.1.0"
  }
}
```

#### scripts

터미털에서 command line으로 명령을 내려 실행하게 할 수있는 스크립트를 작성할 수 있다. 이 스크립트는 보통 환경별로 빌드를 하는 경우 사용한다.

#### dependencies

모듈에 종속적인 모듈들을 나열한다. 이 곳에 종속된 모듈들은 빌드시에 번들링된 js파일에 포함된다. 사용하는 모듈의 종속여부가 불분명할 때는 *사용할 모듈의 document를 보면 어떻게 인스톨할지 설명이 되어 있다.*

```
npm install 모듈
```

#### devDependencies

모듈 개발시에 종속적인 모듈들을 나열한다. 이 곳에 종속된 모듈든은 번들링시에 포함되지 않는다. 단지 개발시 개발자에게 필요한 모듈들을 종속하여 개발할 때 사용한다. eslint가 대표적이다.

```
npm install 모듈 -D
```