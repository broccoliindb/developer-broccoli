---
title: "Trouble Shooting"
displayTitle: "Trouble Shooting"
date: 2020-11-05T12:00:24+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Webpack Issue 처리"
tags: []
weight: 0
disqus_identifier: "1c029fe6d1080c94e06ef247a71723f5"
disqus_title: "Trouble Shooting"
disqus_url: "1c029fe6d1080c94e06ef247a71723f5"
---
## Webpack 이슈

### Webpack: Bundle.js - Uncaught ReferenceError: process is not defined

webpack이 버전업이 되면서 이부분 처리해주는 polyfill을 제거해서 발생한것인지 이유는 잘 모르겠지만....이렇게 undefined 된 에러들의 경우 undefined를 해결해주면 문제가 해결된다.

#### 해결법

✅ webpack.config.js

```js
const path = require("path");
const webpack = require("webpack");

module.exports = {
  ...
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("development"),
    }),
  ],
  ...
};

```