---
title: "Nosql"
displayTitle: "NoSQL"
date: 2020-09-29T21:37:12+09:00
isCJKLanguage: true
draft: false
categories: []
description: "NoSQL의 대한 정리문서"
tags: []
weight: 0
disqus_identifier: "dc7e30dd4e3a44cc09fefca12698c173"
disqus_title: "Nosql"
disqus_url: "dc7e30dd4e3a44cc09fefca12698c173"
---

# NoSQL

Not Only SQL 혹은 non SQL 줄임말

- RDB보다 유연한 모델로 데이터의 저장 및 검색 메커니즘을 제공
  - 유연하기 때문에 확장성과 생산성은 높아지지만 RDB보다 `정합성`이 떨어지는 요인이 됨
  - 정합성을 보완해주는 다른 장치를 함께 사용함
  - 데이터를 nested로 중첩할 수 있다.
  - 운영중에 스키마 변경시 RDB에 비해 비용이 적게 들 수있다. 
- 단순 검색이나 추가 작업에 유리하여 `latency: `와 `throughput` 측면에서 상당한 성능을 냄.  

ℹ️ latency : Time required from Device to the Application Server Or RTT(Round Trip Time : from Device to Server then Back to Device)

ℹ️ throughput : How much data can be transferred from one location to another in a given amount of time

## 정합성 

데이터들의 값은 서로 일치해야한다.

RDB 는 정규화를 통해 모델링을 한다. 따라서 FK를 통해 필요한 데이터를 조인해서 조회해하기 때문에 정합성이 보장된다.
NoSQL은 비정규화를 기본으로 가져간다. 따라서 정합성이 보장되지 않을 수 있다.

## RDB vs NoSQL

||쿼리 데이터 사이즈| 전체 데이터 사이즈 |
|---|---|---|
|RDB|정규화를 통해 데이터가 여러 테이블에 분산되어있어 쿼리수행시 **쿼리당 I/O는 비교적 클 수 있음**| 정합성을 보장하므로 NoSQL보다 **전체데이터 사이즈 증가폭 낮음**|
|NoSQL|정규화보다 비교적 다양한 데이터를 한곳에 모아두고 쿼리를 수행하기에 **쿼리당 I/O는 비교적 작을 수있음**| 데이터 중복허용을 통해 점차 **전체데이터 사이즈 증가**할 수 있음|

## JOIN

RDB와 비슷하게 JOIN 같은 기능을 사용할 수 있다. RDB와 다른점은 RDB는 DB내부에서 조합된 데이터라 할수있다.

- RDB는 low영역의 JOIN이라면, NoSQL에서는 Application 영역의 JOIN이므로 좀 더 성능에 영향이 가기때문에 많이 하는 것은 자제할 필요가 있음.
- 하지만 빈번한 데이터변경이 이루어지는 부분의 경우는 정합성을 위해 사용해야함
