---
title: "Mongodb Install"
displayTitle: "몽고 DB 설치"
date: 2020-09-29T21:38:44+09:00
isCJKLanguage: true
draft: false
categories: []
description: "몽고 DB 설치방법"
tags: []
weight: 0
disqus_identifier: "2a9a194137986a75c2c4ced4aedbd3eb"
disqus_title: "Mongodb Install"
disqus_url: "2a9a194137986a75c2c4ced4aedbd3eb"
---
# mongoDB 설치

- [설치링크 및 메뉴얼](https://docs.mongodb.com/manual/administration/install-community/)
- [설치시 참고자료](https://stackoverflow.com/questions/58034955/read-only-file-system-when-attempting-mkdir-data-db-on-mac)

```
mkdir -p /data/db
mongod

// mkdir: /data/db: Read-only file system 에러가 날 경우
mkdir -p data/db # 또는 원하는 경로에 폴더를 만들면 된다.
mongod --dbpath=/Users/user-name/data/db
```

