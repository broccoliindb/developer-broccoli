---
title: "Mongodb"
displayTitle: "몽고 DB"
date: 2020-09-16T11:33:48+09:00
isCJKLanguage: true
draft: false
categories: []
description: "몽고 DB의 대한 문서"
tags: []
weight: 0
disqus_identifier: "59b93b49c7a75582156af3289058231d"
disqus_title: "Mongodb"
disqus_url: "59b93b49c7a75582156af3289058231d"
---
# 몽고DB

- NoSQL에 속하는 Database
- C++로 작성된 Open Source Document Oriented Database
  - Document Oriented는 데이터 베이스가 데이터를 저장할 때 JSON-like documents로 저장함.
- 생산성이 높고 확장성과 성능도 우수한 편
- 참고링크
  - [mongoDB vs MySQL](https://www.simform.com/mongodb-vs-mysql-databases/)
