---
title: "Mongodb Quickstart"
displayTitle: "몽고 DB Quick Start"
date: 2020-09-29T21:46:39+09:00
isCJKLanguage: true
draft: false
categories: []
description: "몽고 DB의 간단한 사용법"
tags: []
weight: 0
disqus_identifier: "cb254d6f6c39fdbc80479b5e7d29c95d"
disqus_title: "Mongodb Quickstart"
disqus_url: "cb254d6f6c39fdbc80479b5e7d29c95d"
---
# mongoDB Quick Start

## 용어정리

- mongod: MongoDB system을 위한 primary daemon process
- mongo: The mongo shell is an interactive Javascript interface to mongoDB
  - MongoDB 시스템에 접속해 데이터를 조회하거나 변경할수 있는 REPL(Read / Evaluation / Print / Loop)
  - REPL은 단일 사용자의 입력을 받아 이를 실행하고 결과를 사용자에게 반환시키는 단순한 상호작용 프로그램

## Database 생성, 제거 etc

### DB 생성

데이터베이스를 생성하고 만약 이미 존재하면 기존것 사용함.

```mysql
use database_name_to_use 
```

### DB 제거

```mysql
use database_name_to_delete
db.dropDatabase()
```

### 현재 db 확인

```mysql
db
```

### 존재하는 db 리스트 확인

```mysql
show dbs
```

## Collection 생성 및 제거

### Collection 생성

```mysql
db.createCollection("name", {capped: true, size:})
```

- name : 컬렉션 이름
- options {Object} : 생략가능하다.
  - capped {boolean}
  - size {Number byte} : capped가 true이면 설정한다. 컬렉션의 최대 사이즈를 지정한다.
  - max {Number} : collection에 추가할수있는 최대 Document개수를 설정

ℹ️ capped : capped를 true로하면 size를 정한만큼만 공간을 사용한다는 뜻입니다. 할당된 공간을 전부. 보통 로깅용도로 사용한다.

ℹ️ non capped: size에 명시한 공간을 전부 사용했을시 추가로 공간을 할당함.

### Collection 제거

```mysql
db.collection.drop()
```

### Collection 기타 명령어

```mysql
1. collection상태확인
db.collectionName.stats()

2. capped여부확인
db.collectionName.isCapped()

3. convertToCapped
// DO NOT RUN THIS COMMAND IN SHARDED CLUSTERS 
// MongoDB does not support the convertToCapped command in a sharded cluster.

db.runCommand({ convertToCapped: <collection>,
  size: <capped size>,
  writeConcern: <document>,
  comment: <any>
})

```

ℹ️ capped to uncapped : 콜렉션을 복사하는 방법밖에 없음.

## Document 생성 및 제거

### Document 생성

콜렉션을 생성하지 않아도 document를 생성하면 해당 콜렉션이 자동 생성됨.

```mysql
db.collection.insert({document} or [])
db.collection.insertOne()
db.collection.insertMany()
```

### Document 제거

```mysql
db.collection.remove(query, justOne)
```

ℹ️ query: 삭제할 데이터를 식별할 쿼리. {} 넣으면 모든 데이터 삭제한다.

ℹ️ justOne {Boolean} : 생략가능하며 이 값이 true이면 한개의 데이터만 삭제한다.

### Document 조회

```mysql
db.collection.find(query, projection)
db.collection.find({"title": "manual books"}, {writer: true})
```

ℹ️ query: 조회할 데이터를 식별할 쿼리. 아무것도 없거나 {} 넣으면 모든 데이터 조회한다.

ℹ️ projection : 생략가능하며 데이터를 조회할 때  결과값에 보여질 field를 결정. 위에 예에서는 writer 필드를 조회한다.

## Query Selectors

find() 함수의 첫번째 인자인 query object에서 사용되는 연산자

- Comparison: property값이 주어진 값과 비교
- Logical: 여러 조건식을 연결
- Element: property의 타입 또는 존재 유무를 판별
- Evaluation: 주어진 표현식을 평가

### Comparison

|연산자|의미|
|---|---|
|$eq|주어진값과 일치하는|
|$gt|주어진값보다 큰|
|$gte|주어진값보다 크거나 같은|
|$lt|주어진값보다 작은|
|$lte|주어진값보다 작거나 같은|
|$ne|주어진값과 일치하지 않는|
|$in|주어진 배열 안에 속하는|

```mysql
db.posts.find( {
  "likes": { $gte: 50 }
}).pretty()

db.posts.find( {
  "title": { $in: ["mongoDB", "node.js"] }
}).pretty()
```

### Logical

|연산자|의미|
|---|---|
|$or|주어진 조건중 하나라도 true 이면 true|
|$and|주어진 모든 조건이 true이면 true|
|$not|주어진 조건이 false이면 true|
|$nor|주어진 모든 조건이 false 이면 true|

```mysql
db.posts.find( { $and: [
  { "likes": { $gte: 50 }},
  { "title": "mongodb" }}
]}).pretty()

db.posts.find( {
  "title": {
    $not: { $in: ["mongoDB", "node.js"] }
    }}).pretty()
```

### Element

|연산자|의미|
|---|---|
|$exists|특정한 property를 갖고 있는|
|$type|property가 주어진 특정한 타입인|

```mysql
db.posts.find( { “modifiedAt”: { $exists:
true } } )

db.posts.find( { "content" : { $type :
"string" } } )
```

ℹ️ [mongoDB가 제공하는 타입 참고링크](https://docs.mongodb.com/v4.2/reference/operator/query/type/#available-types)

### Evaluation

|연산자|의미|
|---|---|
|$mod|해당 property에 나머지 연산을 수행|
|$regex|해당 property에 정규식으로 판단|
|$text|text search를 수행|
|$where|해당 property에 js expression 평가에 부합하는지 판단|

```mysql
db.posts.find( { likes: { $mod: [ 10, 0 ]
} } )

// for text search
db.posts.createIndex( { content: "text" } )

// 인덱스를 활용해 text search
db.posts.find( { $text: { $search: "coffee"
} } )

db.posts.find( { $where:
“this.content.length > 1000” } )
```

ℹ️ [mongoDB가 제공하는 Evaluation 참고링크](https://docs.mongodb.com/v4.2/reference/operator/query/type/#available-types)


## Projection

find() method의 두번째 파라미터: 쿼리의 결과값에서 보여질 필드를 결정

```mysql
db.posts.find( {} , { “_id”: false,
“title”: true, “content”: true } ).pretty()
```

### Embeded Document 제한해서 조회하기

ℹ️ Embeded Document의 경우 limit값을 지정해서 제한된 수를 조회할수 있다.

```mysql
db.posts.find( { “title”: “GraphQL” }, {
tags: { $slice: 10 } } )
```

### find 관련 자주사용되는 메서드

- sort
- limit
- skip

#### sort

ℹ️ 좋아요 수 기준으로 오름차순, _id 기준으로

```mysql
//내림차순정렬
db.posts.find().sort( { "likes": 1, "_id":
-1 } )
```

#### limit

가져올 데이터의 갯수를 제한한다.

ℹ️  좋아요 수 기준으로 오름차순, _id 기준으로
내림차순 정렬 + 이 정렬 기준으로 2개만 가져온다

```mysql
db.posts.find().sort( { "likes": 1, "_id":
-1 } ).limit(2)
```

#### skip

데이터의 시작부분을 건너뛴다.

ℹ️  좋아요 수 기준으로 오름차순, _id 기준으로
내림차순 정렬 + 이 정렬의 앞 2개를 제외한다

```mysql
db.posts.find().sort( { "likes": 1, "_id":
-1 } ).skip(2)
```

## Document 수정

```mysql
db.collection.update(query, update, options)¶
```

디폴트로 한개의 document만 수정하고 multi true 옵션의 따라 멀티로 수정가능함.

```mysql
db.collection.update(
   <query>,
   <update>,
   {
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>,
     collation: <document>,
     arrayFilters: [ <filterdocument1>, ... ],
     hint:  <document|string>        // Available starting in MongoDB 4.2
   }
)
```

ℹ️ [MongoDB Update 옵션 참고링크](https://docs.mongodb.com/manual/reference/method/db.collection.update/)

|자주사용되는 옵션명| 설명|
|---|---|
|query| 업데이트할 document의 query, find 메서드사용시와 동일|
|update| document에 적용할 변경사항|
|upsert| 만약 true이면 해당 document없을시에 새로 생성한다.|
|multi| 만약 true이면 여러개의 해당 document를 수정함|
|writeConcern| shared clusters나 replica sets을 사용하는 환경에서 document를 쓰거나 수정하는 operation의 전파에 사용|

## 참고링크

- [MongoDB Capped Collection - Create, Check, Uncapping the Capped](https://data-flair.training/blogs/mongodb-capped-collection/)
- [MongoDB Update 옵션 참고링크](https://docs.mongodb.com/manual/reference/method/db.collection.update/)
- [MongoDB WriteConcern 링크1](https://stackoverflow.com/questions/46652028/usage-of-writeconcern-in-mongodb)
- [MongoDB WriteConcern 링크2](https://docs.mongodb.com/manual/reference/write-concern/)