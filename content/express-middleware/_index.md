---
title: "Express Middleware"
displayTitle: "express 미들웨어"
date: 2020-10-07T22:51:37+09:00
isCJKLanguage: true
draft: false
categories: []
description: "자주사용하는 express 미들웨어"
tags: []
weight: 0
disqus_identifier: "f408ce9ac5e589bee28ffc93d02b3188"
disqus_title: "Express Middleware"
disqus_url: "f408ce9ac5e589bee28ffc93d02b3188"
---
# express 미들웨어

express는 unopinionated 프레임워크라고 한다. 쉽게말하면 정해진 답이 없고 상황에 따라 필요에따라 얼마든지 틀이 융통성있게 사용될 수 있다는 의미이다.

덕분에 express는 많은 미들웨어 패키지들을 가지고 있는데 그 중 자주 사용되는 대표적인 미들웨어들은 다음과 같다.
