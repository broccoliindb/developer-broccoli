---
title: "Express Session"
displayTitle: "express-session"
date: 2020-10-07T22:56:34+09:00
isCJKLanguage: true
draft: false
categories: []
description: "사용자 세션ID만을 쿠키에 담아두고 세션정보는 서버사이드쪽에 저장하는 방식"
tags: []
weight: 0
disqus_identifier: "30fe681d4e464612788509ededaf50e5"
disqus_title: "Express Session"
disqus_url: "30fe681d4e464612788509ededaf50e5"
---

# express-session

express-session은 쿠키를 이용하기는 하지만 쿠키에는 아무런 정보도 없는 세션 ID값만을 저장한다. 그리고 세션 정보는 서버쪽에 저장한다.

ℹ️ 반면에 cookie-session은 세션정보자체를 브라우저(클라이언트) 쿠키에 저장한다.

||cookie-session|express-session|
|---|---|---|
|쿠키|사용|사용|
|세션정보|`클라이언트` 쿠키에 저장| `서버사이드`에 저장|
|비교1|백엔드에서 처리하는 부분없음|세션정보는 굉장히 빈번한사용된다. 따라서 `백엔드 서버에 무리가 가지 않도록 처리해야한다.`|
|비교2|세션정보는 `쿠키사이즈(4KB)를 초과할 수 없다.`|용량의 한계가 없다.|
|비교3|클러스터 앱의 경우 `세션정보의 sharing에 대해 고민할 필요가 없다.`|`sharing 세션정보에 대해 고민해야한다.`|
|비교4|클라이언트에 세션정보가 노출되어있기 때문에 `안정성에 매우 신경`써야한다.|좀 더 `안정적`이다.|

### install

```js
npm install express-session
```

ℹ️ express-session 1.5 버전 이후로 cookie-parser가 필요가 없어졌음.

### usage

```js
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}))
```

#### 옵션

|옵션|필수여부|목적|
|---|---|---|
|secret|필수|반드시 dotenv를 사용하여 노출을 하지 않도록 한다. |