---
title: "Vanilla Redux"
displayTitle: "(Basic) Vanilla Redux practice"
date: 2020-11-04T16:47:36+09:00
isCJKLanguage: true
draft: false
categories: []
description: "바닐라 스크립트로 리덕스를 연습하기"
tags: []
weight: 0
disqus_identifier: "afbac11a54905121bd3a944b1d431afb"
disqus_title: "Vanilla Redux"
disqus_url: "afbac11a54905121bd3a944b1d431afb"
---

## Vanilla Counter

- updateCount라는 함수로 상태를 변경해준다.
- updateCount가 없으면 상태변경이 안됨.

![vanillaRedux](/redux/images/vanillaRedux.png)

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div>
      <button class="add">add</button>
      <span class="count">0</span>
      <button class="minus">minus</button>
    </div>
    <script>
      const countEl = document.querySelector(".count");
      const addEl = document.querySelector(".add");
      const minusEl = document.querySelector(".minus");
      let cnt = 0;
      countEl.innerText = cnt;
      const updateCount = () => {
        countEl.innerText = cnt;
      };
      const add = () => {
        cnt++;
        updateCount();
      };
      const minus = () => {
        cnt--;
        updateCount();
      };
      addEl.addEventListener("click", add);
      minusEl.addEventListener("click", minus);
    </script>
  </body>
</html>
```