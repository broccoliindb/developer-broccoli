---
title: "Concept"
displayTitle: "(Basic) Redux 기본 용어정리"
date: 2020-11-04T17:47:02+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Redux의 대한 기본 용어정리"
tags: [redux, subscribe, dispatch, createStore, getState]
weight: 0
disqus_identifier: "58227f337e7f36ae0b760d570feddcae"
disqus_title: "Concept"
disqus_url: "58227f337e7f36ae0b760d570feddcae"
---
## (Basic) Redux 기본 용어정리

```
npm install redux
```

해당 리덕스는 `하나`의 저장소에서 관리되며 `상태를 변화시키는 유일한 방법`은 *action*을 실행하는 것이다.

### createStore

