---
title: "Redux"
displayTitle: "Redux"
date: 2020-11-04T16:35:28+09:00
isCJKLanguage: true
draft: false
categories: []
description: "자바스크립트로 state를 관리하는 방법"
tags: [redux, state]
weight: 0
disqus_identifier: "d0f4c1804e4d7a69fe559ef2e7513a27"
disqus_title: "Redux"
disqus_url: "d0f4c1804e4d7a69fe559ef2e7513a27"
---
## Redux

> Redux is a predictable state container for JavaScript apps

리덕스는 리액트로부터 종속하는 개념은 아니다. 상태관리를 위한 개념이라고 생각하면되며 바닐라던 다른 프레임워크에서든 적용할 수 있는 개념이다.

리덕스의 대한 [잘 정리된 문서](https://www.npmjs.com/package/redux)는 공식 사이트에 잘 정리되어있다.  

basics부터 advanced까지 시간 날때 마다 정리할 예정이다.
