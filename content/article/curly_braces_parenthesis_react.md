---
title: "Curly_braces_parenthesis_react"
displayTitle: "리액트속 {}: curly braces, (): parenthesis"
date: 2020-10-27T13:31:40+09:00
isCJKLanguage: true
draft: false
categories: []
description: "리액트에서 사용되는 {}: curly braces, (): parenthesis 의 대한 정리"
tags: ["curly braces", "parenthesis", "react"]
weight: 0
disqus_identifier: "832ee0b6400e4133dcd681595542ce0d"
disqus_title: "Curly_braces_parenthesis_react"
disqus_url: "832ee0b6400e4133dcd681595542ce0d"
---
## 리액트에서 사용되는 {}: curly braces, (): parenthesis 의 대한 정리

해당 문서는 [Curly braces { } versus parenthesis ( ) in ReactJS](https://medium.com/@leannezhang/curly-braces-versus-parenthesis-in-reactjs-4d3ffd33128f)에서 참고한 자료입니다.

### { } in JSX

JSX의 { }는 JSX의 compilation중에 javascript의 expression를 evaluation 해준다. 따라서 react의 javascript를 사용할 때는 항상 { }안에서 사용하도록 하자.

ℹ️ evaluate는 어떤 프로그램이 결과를 출력하기 위해 필요한 자료를 모으고 분석하는 방법들을 의미함.

ℹ️ expression은 variables, function, object 등 어떤 값을 도출하는 모든 코드가 허용됨.

```react
class PopUp extends React.Component {
  // es7 way of setting default state
  state = {
    visible: true;
  }
  render() {
    return <Modal onClose={this._handleClose}/>;
  }
  _handleClose = () => {
    this.setState({ visible: false });
  }
}
```

### ( ) parenthesis usage

( ) 이것은 arrow function 을 사용할 때 종종 볼 수 있다.

arrow function 사용시 리턴값이 object이고 리턴되는 라인이 2줄이상이라면 반드시 ( )을 사용해야한다. 아래 두개는 동일한 표현이다. 한줄이라면 감싸지 않아도 된다.

```react
() => ({ name: 'Amanda' })  // Shorthand to return an object
```

```react
() => {
   return { name : 'Amanda' }
}
```
