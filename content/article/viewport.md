---
title: "Viewport"
displayTitle: "viewport"
date: 2020-11-10T10:28:59+09:00
isCJKLanguage: true
draft: false
categories: []
description: "meta viewport의 대한 문서"
tags: []
weight: 0
disqus_identifier: "ac2085edbb30ae4f59ef3eaac24a7d0e"
disqus_title: "Viewport"
disqus_url: "ac2085edbb30ae4f59ef3eaac24a7d0e"
---

## viewport

viewport는 user's visible area이다.

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
```

### width=device-width

page의 width를 device width에 맞게 초기설정을 해준다.

### initial-scale=1.0

page가 browser에 의해 초기 로드될때 zoom level을 세팅해준다.

![viewport](/article/images/viewport.png)