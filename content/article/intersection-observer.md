---
title: "Intersection Observer"
displayTitle: "Intersection Observer API"
date: 2020-11-10T11:38:04+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Intersection Observer API의 대한 문서"
tags: ["Intersection Observer"]
weight: 0
disqus_identifier: "166e183215b8a05cc661c01a05434be9"
disqus_title: "Intersection Observer"
disqus_url: "166e183215b8a05cc661c01a05434be9"
---

## Intersection Observer API

<!-- [Intersection Observer API를 사용하지 않은 상태의 Infinite Scroll 구현](https://codesandbox.io/s/day-nine-solution-6pwcl?file=/src/useInfiniteScroll.js) -->

[원본문서링크](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API)

Intersection Observer API는 타겟팅이 되는 element의 부모 element 나 document의 viewport와 타겟팅과의 교차점 변경을 비동기적으로 관찰하는 방법을 제시해주는 API이다.
따라서 아래와 같은 항목들을 구현할 때 굉장히 유용하게 사용할 수 있다.

- Lazy loading
- Infinite scrolling
- Reporting of visibility of advertisements
- Deciding whether or not to perform tasks or animation processes

Intersection Observer는 타겟팅이 되는 element가 상위 element 혹은 viewport와의 교차점에 진입하면서부터 나올때까지 callback을 실행하기 때문에 main thread에게 부담을 주지 않는다. 따라서 브라우저가 버벅거리거나한 사용자 경험이나 기존방식의 기능개선으로 인한 개발자의 부담을 줄어준다.

### Intersection observer concepts

타겟팅이 되는 element가 viewport나 설정된 element와의 교차환경이 변경될때(*whenever the visibility of the target element changes:* `이말은 root와 교차되면서 타겟의 보이는 면적이 변경이 생길때마다라는 의미`)마다 callback이 실행된다.

```js
const opts = {
  root: document.querySelector('.scrollArea'),
  rootMargin: '0px',
  threshold: 1.0
}

let observer = new IntersectionObserver(callback, options)
```

#### root

이때 이 viewport나 설정된 부모 element를 *root*라고 한다.

> *만약 특정화된 root를 설정하고 싶다면 null 아니라 명시적으로 해라. 그러면 지정된 root와의 교차이외에는 동작하지 않는다.*

만약 *root: null*이라면 root는 디폴트로 타겟의 가장 가까운 scrollable ancestor를 가리키거나 없으면 viewport를 가리키게 된다

#### rootMargin

root주변의 margin을 의미한다. 단위는 px도 되고 %도 된다.

#### threshold

single number나 array된 numbers를 넣으면 된다.

threshold 1.0이 의미하는 것은 타겟이 100% 다보일때만 callback을 실행하라는 의미이다. 반대로 0이면 안보일때 callback을 실행하라는 의미이다. `즉 언제 callback을 실핼할것인지 결정해주는 놈이다.`

### Intersection observer Usage

```js
handlerIntersection(entries, observer) {
  ...
}

const opts = {
  root: document.querySelector('.scrollArea'),
  rootMargin: '0px',
  threshold: 1.0
}

let observer = new IntersectionObserver(handlerIntersection, opts)

const target = document.querySelector('.target')

observer.observe(target)
```

사용법은 언뜻보면 어려워보이지만 한번 이해하면 보이는 것만큼 어렵지 않다.

- IntersectionObserver를 생성해서 observer 인스턴스를 만든다.
- 이때 해당 인스턴스의 callback 과 options을 미리 정의한다.
- 타겟이 될 엘리먼트를 observer인스턴스의 observe 함수 파라미터로 넣어준다.

위와 같은 순서로 구현을 하면되고 아래에는 callback의 첫번째 파라미터인 *IntersectionObserverEntry object*의 대해 알아보자.

#### IntersectionObserverEntry

callback handler 의 첫번째 파라미터인 entries는 *IntersectionObserverEntry* object인데 이것은 타겟과 root가 교차되는 순간을 묘사하는 object이다. 따라서 그 묘사에 해당하는 정보들을 담고 있다.

##### ℹ️ IntersectionObserverEntry.boundingClientRect

타겟의 bounds rectangle 정보(DOMRectReadOnly)를 리턴한다고 하는데 실제로 측정해보니 *viewport상의 타겟*의 top(y), left(x), right, bottom 정보와 target의 width, height 정보를 리턴한다.

`다시말하면  target.getBoundingClientRect()의 값을 리턴해준다.`

##### ℹ️ IntersectionObserverEntry.intersectionRatio

이것이 의미하는 것은 타겟과 root의 교차 비율을 의미하고 교차의 *가장 윗부분부터 가장 밑단까지 1 ~ 0 을 가리킨다.*

>`바꿔이야기하면 타겟이 root와 교차하기 직전 타겟전체가 다보이면 1, root에 다 교차되어 하나도 안보이면 0을 의미한다고 생각하면 된다.`

##### ℹ️ IntersectionObserverEntry.intersectionRect

타겟의 visible area의 DOMRectReadOnly 값을 리턴한다.

##### ℹ️ IntersectionObserverEntry.isIntersecting

원문그대로 옮기면 아래와 같다.
> A Boolean value which is true if the target element intersects with the intersection observer's root. If this is true, then, the IntersectionObserverEntry describes a transition into a state of intersection; if it's false, then you know the transition is from intersecting to not-intersecting.

근데 실제로 테스트를 해보니 threshold 를 지나면 false 하나라도 threshold 부분이 남아있는 부분이 있다면 true가 됨.

##### ℹ️ IntersectionObserverEntry.target

타겟 element

##### ℹ️ IntersectionObserverEntry.time

원문을 옮기면 아래와 같다.

> A DOMHighResTimeStamp which indicates the time at which the target element experienced the intersection change described by the IntersectionObserverEntry. The time is specified in milliseconds since the creation of the containing document.

타겟이 root와 교차될때 timestamp를 가리키는 것인데, 단위는 milliseconds이고 기준시간은 containing document 가 생성될 때 이후라고 하는데 document가 생성될때를 기준으로 하는 것으로 생각한다.
