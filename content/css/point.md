---
title: "Point"
displayTitle: "CSS selector point"
date: 2020-11-11T19:36:41+09:00
isCJKLanguage: true
draft: false
categories: []
description: "css 스타일 우선 적용의 대한 포인트"
tags: ["css basic"]
weight: 0
disqus_identifier: "59d9d04d0c3f37516556e7f9fd9e4cfc"
disqus_title: "Point"
disqus_url: "59d9d04d0c3f37516556e7f9fd9e4cfc"
---
## Selector Point

css style의 포인트의 따라 적용되는 스타일이 다르다.

|style 적용방법 |점수|비고|
|---|---|---|
|Inline Style| 1000pt||
|ID Selector| 100pt||
|Class, Attributes, Pseudo-Classes| 10pt||
|Elements, Pseudo-Elements| 1pt||

나중에 스타일이 먹지 않는 경우 계산을 따져봐서 총 합으로 계산 하면 된다.

ℹ️ * {} : 0pt
