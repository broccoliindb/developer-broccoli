---
title: "Eslint"
displayTitle: "eslint"
date: 2020-10-05T16:37:21+09:00
isCJKLanguage: true
draft: false
categories: []
description: "eslint의 설치와 사용법을 확인"
tags: [vscode with eslint]
weight: 0
disqus_identifier: "fd82bc302fe69c4af9bcc449c8cb38a9"
disqus_title: "Eslint"
disqus_url: "fd82bc302fe69c4af9bcc449c8cb38a9"
---
# ESLINT

eslint는 코딩을 하면서 발생할 수있는 human error를 줄이도록 도와주는 linter.  [🔗 공식사이트링크 ](https://eslint.org/docs/user-guide/getting-started)

eslint는 큰 plug-in들이 있어서 사용자에 따라 얼마든지 다른 linter를 적용할 수 있다는 점에서 많이 사용되고 있다.

## eslint Quick Start

### install

```js
  npm install eslint -D
  
  // package.json이 이미 있다는 가정하에서...
  npx eslint --init
```

ℹ️ eslint를 설치할 때는 global로 설치하지 말고 꼭 로컬로 설치하는게 좋다. eslint뿐만아니라 왠만한 모든 모듈들은 꼭 로컬로만 설치하는게 좋다.

global로 설치할 때는 해당 패키지 프로그램의 의해서 여러 다른 파일시스템에서 사용될 수 있기 때문에 보안문제로 인해 꼭 로컬로 설치하여 제한적으로 실행시키자

### options

터미널을 통해 여러 옵션이 제공되는데 개인에 맞게 선택하면된다.

- To check syntax, find problems, and enforce code style
- CommonJS (require/exports)
- None of those for Project use
- No for typescript
- Node for code run
- Use a pupular style guide
- Standard
- Javascript for config file
- Yes for npm install

설치하면 아래와 같은 음영들이 설치가 된다.

{{< highlight "js" "linenos=table,hl_lines=3-7,linenostart=1" >}}
  "devDependencies": {
    "eslint": "^7.10.0",
    "eslint-config-standard": "^14.1.1",
    "eslint-plugin-import": "^2.22.1",
    "eslint-plugin-node": "^11.1.0",
    "eslint-plugin-promise": "^4.2.1",
    "eslint-plugin-standard": "^4.0.1",
    "nodemon": "^2.0.4"
  }
{{< /highlight >}}

그럼 프로젝트 폴더에 .eslintrc.js가 설치된것을 확인할 수 있다. 터미널에서 아래와 같이 명령하면 잘 적용된것을 확인할 수 있다.

```js
npx eslint <file path>
```

visual code에서는 `eslint extension`을 설치 해 줘야 **룰 위반들을 확인 할 수 있다.**
