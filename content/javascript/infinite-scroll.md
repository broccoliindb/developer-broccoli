---
title: "Infinite Scroll"
displayTitle: "Infinite Scroll"
date: 2020-11-09T15:07:57+09:00
isCJKLanguage: true
draft: false
categories: []
description: "스크롤링으로 페이징하기"
tags: ["infinite scroll", IntersectionObserver, vanilla, react]
weight: 0
disqus_identifier: "5e7543645eb38b2c48eedc88b394d2d6"
disqus_title: "Infinite Scroll"
disqus_url: "5e7543645eb38b2c48eedc88b394d2d6"
---
## Infinite Scroll

Pagination을 하는 여러 방법중에 하나이다. 많은 리스트를 한꺼번에 로드를 하면 사용자 경험에 좋지 않기 때문에 일정 필요한 부분만 로드하도록 하는 방법

offset pagination 과 cursor pagination 방법이 있는데, 방식의 구분은 백엔드 구현의 차이가 있다. 대규모 데이터의 경우 cursor pagination을 권장함.

ℹ️ offset Pagination

페이지 단위별로 필요정보를 요청하는 방법

```js
params = {
  offset: 1,
  limit: 10
}
```

ℹ️ cursor Pagination

클라이언트가 가져간 마지막 row의 키값을 기반으로 이용한다.

### IntersectionObserver 을 이용하기

pagination을 스크롤을 통해 구현해보려고한다. vanilla 버전과 react 버전으로 2가지 버전으로 구현하려고 함. 구현 방식은 동일하게 IntersectionObserver를 사용할 것이다.

ℹ️ 사용할 API : [Movie API](https://yts.mx/api)

해당 API는 주소가 자주 변경될 여지가 많음. 해당 api는 offset방식으로 구현된 api지만 스크롤로 페이징을 구현할 때는 offset이든 cursor든 사실 앞단에서는 별 차이가 없어 보임.

#### 1. Vanilla version

실제 소스는 아래 버튼을 클릭하면 확인가능하다.

|함수|목적|
|---|---|
|init|초기설정을 해주는 함수|
|compositeMovieList|영화리스트를 조회하고 element에 set하는 부분|
|getMovieList|영화리스트를 조회하는 부분|
|setMovieList|조회한 영화리스트를 element에 바인딩하는 부분|
|setInfiniteScroll|IntersectionObserver를 생성하고 그에 해당하는 handler와 observer를 설정해주었다.|
|getFetch|영화리스트 fetch 함수|

✅ index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
    <title>Infinite Scroll</title>
    <h1>Movie List</h1>
    <div class="container"></div>
    <div class="loader loading">Loading...</div>
  </head>
  <body>
    <script src="./index.js"></script>
  </body>
</html>
```

✅ style.css

```css
.loader {
  visibility: hidden;
}

.loader.loading {
  visibility: visible;
}

```

{{< show >}}
index.js 코드보기
{{< /show >}}

{{< hide >}}
```js
const container = document.querySelector('.container')
const loader = document.querySelector('.loader')

const getFetch = (detailedUrl, params) => {
  const baseUrl = 'https://yts-proxy.nomadcoders1.now.sh'
  const getQueryString = (params) => {
    const esc = encodeURIComponent
    return Object.keys(params)
      .map((k) => esc(k) + '=' + esc(params[k]))
      .join('&')
  }
  const options = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return fetch(`${baseUrl}${detailedUrl}?${getQueryString(params)}`)
}

const getMovieList = async (page) => {
  const movieData = []
  const params = {
    page: page,
    limit: 50
  }
  let error = null
  let loading = true
  if (!loading) {
    loader.classList.remove('loading')
  } else {
    loader.classList.add('loading')
  }
  try {
    const response = await getFetch('/list_movies.json', params)
    if (response.ok) {
      const {
        data: { movies }
      } = await response.json()
      movieData.push(...movies)
    }
  } catch (err) {
    error = err
    console.log(err)
  } finally {
    loading = false
  }
  return {
    loading,
    error,
    movieData
  }
}

const compositeMovieList = async (page) => {
  const { loading, error, movieData } = await getMovieList(page)
  setMovieList(loading, error, movieData)
}

const setInfiniteScroll = (page) => {
  const handleObserver = (entries, observer) => {
    const { isIntersecting } = entries[0]
    if (isIntersecting) {
      compositeMovieList(++page)
    }
  }
  const opts = {
    root: null,
    rootMargin: '0px',
    threshold: 1.0
  }
  const observer = new IntersectionObserver(handleObserver, opts)
  observer.observe(loader)
}

const setMovieList = (loading, error, movieData) => {
  const fragment = new DocumentFragment()

  if (movieData && movieData.length) {
    movieData.forEach((i) => {
      const li = document.createElement('li')
      li.innerHTML = i.title
      fragment.appendChild(li)
    })
    container.appendChild(fragment)
    if (!loading) {
      loader.classList.remove('loading')
    } else {
      loader.classList.add('loading')
    }
  }
}

const init = async () => {
  let page = 1
  await compositeMovieList(page)
  setInfiniteScroll(page)
}

window.addEventListener('load', init)
```

{{< /hide >}}

#### 2. React version

리액트 버전도 구현 방식은 동일하다 다만 useState와 useEffect의 이해가 어느정도 필요하다.

아래는 IntersectionObserver 관련 부분만 발췌한 소스이다.

page는 state에 등록해둔상태이고 observer는 바닐라와 마찬가지로 loading부분에 참조해주었다.

```react
  const handleObserver = (entities, observer) => {
    const { isIntersecting, intersectionRatio } = entities[0]
    if (isIntersecting) {
      setPage((p) => p + 1)
    }
  }

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: '0px',
      threshold: 0
    }
    const observer = new IntersectionObserver(handleObserver, options)
    observer.observe(loadingRef.current)
  }, [])

  useEffect(() => {
    getMovies(page)
  }, [page])
```
