---
title: "Rendering Process"
displayTitle: "Rendering Process"
date: 2020-11-11T19:51:46+09:00
isCJKLanguage: true
draft: true
categories: []
description: "HTML/CSS가 화면으로 다시 태어나는 과정"
tags: []
weight: 0
disqus_identifier: "d1573ac02048c792111dddca702ce521"
disqus_title: "Rendering Process"
disqus_url: "d1573ac02048c792111dddca702ce521"
---

https://developers.google.com/web/fundamentals/performance/critical-rendering-path

## Rendering Process

HTML/CSS가 화면으로 다시 태어나는 과정

![dom-construct](/html/images/dom-constuct.png)

![render-tree](/html/images/render-tree.png)

렌더트리에는 설사 DOM에 있더라도 (ex: display: none) 렌더될 필요가 없는 아이템들의 경우 렌더트리에 올라오지 않는다.

layout
실제 viewport에 따라 사이즈를 비교해서 render tree에 올라온 엘리먼트들이 사이즈 계획이 짜여져서 box model로 도출된다.

box model: 브라우저가 렌더트리를 생성하고 그 렌더트리가 화면에 어떻게 보여질지 그 구조를 짠 결과물: layout단계에서 box model이 나온다., reflow를 다시한다.

=> 레진코믹스 같은 곳.

painting: 계산된 결과물을 그리는 것.

udacity: website performance optimization

### render blocking resources

html, css render blocking resources라고 한다. 왜냐면 화면을 보여주기 위해서는 html, css가 필수적인 자원이기 때문에

그래서 브라우저가 최대한 빨리 CSSOM 을 완성하게 도와 주기 위해서 css는 보통 html에 상단에 넣는다.

### Adding Javascript

만약 cssom이 완성되지 않았는데 js가 실행되면 자바스크립로 css를 수정하게 되면 다시 cssom을 수정해야하고 성능에 좋지 않기 때문에
그래서 브라우저에서는 보통 cssom이 완성되기전에는 자바스크립트를 실행시키지 않는다.

그리고 dom construction은 script태그를 만나서 실행중인 동안에는 멈춘다.

그래서 css는 헤드에 자바스크립트는 바디 최하단에 주로 넣는다.

예외가 있다면 비동기적으로 돔을 수정하지 않고 동작하는 외부소스들의 경우 head에 넣어두 된다. google analytics 같은 것들...

### critical rendering path

html/css/js 자원들을 브라우저가 최대한 빠르고 효율적으로 보여주게 하기 위한 과정
웹사이트의 성능을 최적화 시키는 과정

