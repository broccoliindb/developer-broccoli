---
title: "Document Object Model"
displayTitle: "DOM의 관한 정리"
date: 2020-11-10T14:54:52+09:00
isCJKLanguage: true
draft: false
categories: []
description: "DOM이란 무엇인가?"
tags: [DOM]
weight: 0
disqus_identifier: "6b6e9f5c22ab2566e579ea9b6eebde08"
disqus_title: "Document Width Height Summary"
disqus_url: "6b6e9f5c22ab2566e579ea9b6eebde08"
---
## DOM 관한 정리 문서

### DOM이란?

>a cross-platform and language-independent interface that treats an XML or HTML document as a tree structure

- DOM은 `트리구조로 만들어진 하나의 layer`라고 생각할 수 있다.
  - 프로그래밍 언어 -> DOM -> Documents
- DOM 에서 제공하는 API를 통해 HTML or XML로 된 Document를 자바스크립트로 접근하여 개발자가 DOM을 조작하거나 다룰수 있다.
- 브라우저에서 제공해주는 것이다.

### DOM 구조

![dom](/html/images/DOM-model.svg)

즉, 개발자 모드에서 확인할 수 있는 아래 이미지가 DOM이라고 보면 된다.

![dom](/html/images/dom.png)

### Node

DOM은 트리구조로 되어 있는데 이때 이 트리를 이루는 하나하나의 단위가 노드이고 이런 노드는 여러 종류가 있다.

![nodetypes](/html/images/nodetypes.png)

`이때 가장 중요하고 많이 사용하는 노드가 element node`(*태그라고 생각하면 된다.*)이다. 이것들은 attribute 노드도 포함하고 있고 text node도 포함하고 있다.

따라서 element라는 개념보다 상위라고 볼 수 있다.

### Node vs Element

Node는 Element보다 상위개념이다.

- [Node API](https://developer.mozilla.org/en-US/docs/Web/API/Node)
- [Element API](https://developer.mozilla.org/en-US/docs/Web/API/Element)
