---
title: "Document Etc"
displayTitle: "Document의 관한 기타자료"
date: 2020-11-11T13:58:55+09:00
isCJKLanguage: true
draft: false
categories: []
description: "Document의 관한 기타정보문서"
tags: []
weight: 0
disqus_identifier: "60be3219b68dddabb3c0a74e74522570"
disqus_title: "Document Etc"
disqus_url: "60be3219b68dddabb3c0a74e74522570"
---
## Documents의 관한 기타 정보문서

### Document.documentElement

- root element of document

![documentElement](/html/images/documentElement.png)

### Element.scrollTop

- a measurement of the distance from the element's top to its topmost visible content.

target element의 상단에서 viewport top까지의 거리

![scrollTop](/html/images/scrollTop.png)

### Element.scrollHeight

- a measurement of the height of an element's content, including content not visible on the screen due to overflow.

target이 가지고 있는 스크롤 가능의 모든 높이. 보이지 않는 부분도 포함한다.

### window

[window MDN 참고자료](https://developer.mozilla.org/en-US/docs/Web/API/Window)

- window는 브라우저 하나하나의 창을 의미한다.
- window라는 변수도 있는데 그것은 자바스크립트로 접근할 수 있는 전역변수이다.
- window.innerHeight: *렌더링 된 경우 가로 스크롤 막대를 포함하여 브라우저 창의 콘텐츠 영역 높이를 가져옵니다*
- window.innerWidth: *렌더링 된 경우 세로 스크롤 막대를 포함하여 브라우저 창의 콘텐츠 영역 너비를 가져옵니다.*

