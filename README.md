# Developer Broccoli

> 관심이 가는 기술영역이나 TIL 에 해당하는 정보들을 작성하는 블로그입니다. javascript와 관련된 기술은 프론트 백 가릴것 없이 다 관심이 있으나 현재 프론트쪽에 관련 기술셋에 더 집중하고 있습니다.

https://developer-broccoli.netlify.app/

![coverimage](./cover.png)

## Tech Set

|기술|능숙정도|비고|
|---|---|---|
|html|중상||
|css|중상|scss도 활용가능|
|javascript|중상||
|c#|중상|의료계통 응용프로그램 제작시 wpf와 함께 사용했음|
|wpf|중상|의료계통 응용프로그램 제작시 활용했으나 앞으로 관심을 둘 예정은 없음|
|hugo|중상|정적사이트 제작 프레임워크로서 기술블로그를 만들시 보통 활용함|
|eslint|중상|가능한 항상 사용하고 있으며 보통 standard 룰을 사용한다|
|ember|중상|SPA 프레임워크의 하나로 업무상 익히고 사용해본 경험은 있으나 앞으로 관심을 둘 예정은 없음|
|npm|중상|주로 사용하는 package manager로서 익숙함|
|yarn|중상|자주 사용하던 package manager라 익숙하지만 보통 npm을 선호함|
|react|중|클래스형 컴포넌트 함수형 컴포넌트 모두 사용가능하며 원하는 기능 구현에 문제 없음|
|jenkins|중|bash shell을 사용해서 파이프라인 제작 가능, 필요한 옵션은 필요시 찾아서 습득중|
|oracle|중|의료계통 응용프로그램 제작시 사용했음. DML관련 쿼리 사용에 아주 능숙함|
|regex|중하|기본적인 정규식 개념과 사용법을 알고 있으며 필요시 만들어서 사용가능|
|ubuntu|중하|기본 베이스 운영시스템으로 사용함. bash shell 사용|
|babel|중하|필수적인 옵션개념 및 사용방법 인지하고 있음|
|webpack|중하|필수적인 옵션개념 및 사용방법 인지하고 있으며 필요에 따라 옵션을 습득중|
|gulp|하|사용방법은 알지만 webpack으로 노선 바꿈|
|nodejs|하|nodejs의 대해 깊이파본적은 없으나 필요에 의해 검색해서 지식을 습득중|
|express|하|rest api를 만들수는 있으나 백엔드포지션의 실무 경험은 없음|
|mongodb|하|실무 경험 없음|
|curl|하|터미널로 테스트시 가끔 사용함|
|redux|습득예정||
|typescript|습득예정||
|nextjs|습득예정||
|nestjs|습득예정||
|figma|습득예정||
|webflow|습득예정||

## 블로그 제작 기술

- hugo : 정적사이트 생성 프레임워크
- css
- javascript
- flexSearch.js : 검색서비스
- disqus : 댓글서비스

## 빌드

- gulp를 이용해서 자동 빌드 및 배포 파이프라인 구현
- netlify를 이용한 배포

```
git push origin master
```

